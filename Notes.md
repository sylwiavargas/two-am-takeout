This file is for taking notes as you go.

# Process notes

## VIDEO LINKS
- [setup and first deliverable](https://youtu.be/q_kzHKz0uiw)
- [second and third deliverable](https://youtu.be/HaqXYzAIXZ0)
- [preventing empty messages from being sent + security threat](https://youtu.be/UxKkTSH6aUw)

## SETUP
0. read the readme
1. get started with gitlab + go + vue (VSCode extensions)
2. manage my anxiety connected to the unknown by just getting a feel of vue and go


## TASKS
### 1. ✅ Failing backend test
1. There is a test for the backend that is failing, fix the code so it passes.
    - read the test suite to get a feel of the syntax
    - google or park the questions (not go down the rabbit hole but still know what you don't know)
    - the problem seems to be we are testing a string without lowercasing it but we are comparing it to lowercased v
    - solution: `strings.ToLower(respString)`
    - I googled: "go how to downcase a string" and I used [GeeksForGeeks page](https://www.geeksforgeeks.org/how-to-convert-a-string-in-lower-case-in-golang/) as a resource

## 2. Wrong assumptions, poor tests
2. There is a test for the backend where the assumptions about what needs to be tested are incorrect. The test passes, but doesn't guarantee compatibility with how the frontend expects to communicate. You need to think about how the two interact and are compatible, and change the test code to match.
- during my lunch break, I watched on 2x speed [this intro to websockets](https://www.youtube.com/watch?v=8ARodQ4Wlf4) and took [notes](#notes-from-the-websockets-video)
- my understanding of the assumptions:
     - the messages are text-formatted
     - the messages go through
     - the messages can be read
     - the messages can be read/displayed/are encoded? correctly
- I feel like I have too many gaps in my non-existing go knowledge to make sense of this task; I would take a quick intro to go course and/or talk to some go-person to see what I am not understanding; having worked as a mentor for 2+ years, I feel like I'm good with asking questions to determine what it is that I don't know
- I feel like `Vue.chat:117` might be a clue ("`backend check failed, response code was ${res.status}, data was ${res.data}`")
    - I fixed it by using debugger and changing `res.data.ok` to `res.data.OK`

### ✅ Frontend
3. In the frontend, make it so the user can enter a name that gets sent with every message and displayed to other users.
- ✅ create an input (check how an input is created for the message) 
- ✅ send the username together with the message (check how the message is being sent) 
    - on enter keyup -> send an object (currentMessage + currentName)
    - change the pars in `sendMessage` function
    - test a bit
    - this triggered an error on the backend:`websocket: the client is not using the websocket protocol: 'upgrade' token not found in 'Connection' header` <- to be investigated
        - maybe it's the CORS extension that I have? The error disappeared when I disabled it
- ✅ display the name on the frontend
    - add the date of a message
    - vuetify it up
- ✅ delete the message input after it has been sent

## Further questions and action steps
- **to learn**:
    - read [go naming conventions](https://golang.org/doc/effective_go#names)
    - read more about [runes, strings and bytes](https://blog.golang.org/strings) -> possibly refactor line 56 to use bytes instead
    - what is undertood by scheme in a go request?
    - why `maij_test.go:60` is pointing to `/ws` and not to `/health`?
    - using debugger in Vue
    - ✅ how to check if the data is received by the backend (how?)
        - I inserted a log in `client.go:103` and saw a byte log whenever I sent a message from the frontend

- **to do**:
    - ✅ submit a PR about the tasks number 2 (is: "what needs to be tested is incorrect", shoud be: "what needs to be tested are incorrect" ) 
    - ✅ add self-clearing of the input field(s) 
    - ✅ prevent empty messages from being sent/displayed because of bad ux and unnecessary calls
    - ✅ add self-clearing of the input field(s) 
    - ✅ check/debug why the backend is sending a 200OK code? Why is the data `undefined` 
    - ✅ `client.go:35-37`, the comment about CORS -- read more why it's set up this way
        - 🔻 SAFETY HAZARD: 🔻 https://flaviocopes.com/golang-enable-cors/ 
        - ^ my assumption is that this is only for the assessment but I know that in Rails I should never allow all clients to make requests so let's talk about it

### Notes from the websockets video
- websockets is an http upgrade: using the tcp connection, it allows real-time communication, provides automatic caching
- well-standarized and widely-supported
- does not follow/communicate with RESTful conventions / Auth
- only sends the headers once so there's no overhead when sending data
- possibly, there's a performance optimization opportunity if you remove headers from the request (learn more about it)
- chrome devtools has a WS tab option
- one of the most popular libraries is socketIO:
    - js, includes fallbacks and auto-reconnection, connection handlers; 
    - the syntax seems straightforward
    - namespacing makes it possible to send a message to a group of people connecting through one route
- overhead: 2mb per message